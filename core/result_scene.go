package core

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
)

type ResultScene struct {
	cards []Card
}

func (s *ResultScene) Update(r *ebiten.Image, m *SceneManager) error {

	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		m.GoTo(&TitleScene{})
		return nil
	}

	drawTextCenter(r, "RESULT PAGE", 0, 75, 2, green)

	drawTextCenter(r, "press space key", 0, 225, 1, green)

	return nil
}
