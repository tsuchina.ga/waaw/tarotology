package core

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
)

type TitleScene struct {
}

func (s *TitleScene) Update(r *ebiten.Image, m *SceneManager) error {

	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		m.GoTo(&WishScene{})
		return nil
	}

	drawTextCenter(r, "Tarotology", 0, 100, 2, green)
	drawTextCenter(r, "press space key", 0, 225, 1, green)

	return nil
}
