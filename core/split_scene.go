package core

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"time"
	"strconv"
)

type SplitScene struct {
	start time.Time
	end   time.Time
}

func (s *SplitScene) Update(r *ebiten.Image, m *SceneManager) error {

	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		m.GoTo(&ResultScene{})
		return nil
	}

	drawTextCenter(r, "time diff", 0, 75, 2, green)
	drawTextCenter(r, strconv.FormatInt(s.end.UnixNano() - s.start.UnixNano(), 10), 0, 125, 2, green)

	drawTextCenter(r, "press space key", 0, 225, 1, green)

	return nil
}
