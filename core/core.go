package core

import (
	"github.com/hajimehoshi/ebiten"
	"fmt"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

const (
	ScreenWidth  = 400
	ScreenHeight = 300
)

type Core struct {
	isDebug      bool
	sceneManager *SceneManager
}

func (c *Core) Update(r *ebiten.Image) error {
	if c.sceneManager == nil {
		c.sceneManager = &SceneManager{}
		c.sceneManager.GoTo(&TitleScene{})
	}

	if c.isDebug {
		ebitenutil.DebugPrint(r, fmt.Sprintf("fps: %f", ebiten.CurrentFPS()))
	}

	c.sceneManager.Update(r)
	return nil
}

func (c *Core) DebugOn() {
	c.isDebug = true
}

func (c *Core) DebugOff() {
	c.isDebug = false
}
