package core

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"time"
)

type WishScene struct {
}

var split *SplitScene

func (s *WishScene) Update(r *ebiten.Image, m *SceneManager) error {

	if split == nil {
		split = &SplitScene{start: time.Now()}
	}

	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		split.end = time.Now()
		m.GoTo(split)
		split = nil
		return nil
	}

	drawTextCenter(r, "remember", 0, 75, 2, green)
	drawTextCenter(r, "the", 0, 125, 2, green)
	drawTextCenter(r, "wish", 0, 175, 2, green)

	drawTextCenter(r, "press space key", 0, 225, 1, green)

	return nil
}
