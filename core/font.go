package core

import (
	"image/color"
	"log"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/examples/resources/fonts"
	"github.com/hajimehoshi/ebiten/text"
)

const (
	arcadeFontBaseSize = 12
)

var (
	arcadeFonts map[int]font.Face
)

func getArcadeFonts(scale int) font.Face {
	if arcadeFonts == nil {
		tt, err := truetype.Parse(fonts.ArcadeN_ttf)
		if err != nil {
			log.Fatal(err)
		}

		arcadeFonts = map[int]font.Face{}
		for i := 1; i <= 4; i++ {
			const dpi = 72
			arcadeFonts[i] = truetype.NewFace(tt, &truetype.Options{
				Size:    float64(arcadeFontBaseSize * i),
				DPI:     dpi,
				Hinting: font.HintingFull,
			})
		}
	}
	return arcadeFonts[scale]
}

func textWidth(str string) int {
	b, _ := font.BoundString(getArcadeFonts(1), str)
	return (b.Max.X - b.Min.X).Ceil()
}

var green = color.NRGBA{0x00, 0xff, 0xff, 0xff}

func drawTextCenter(rt *ebiten.Image, str string, x, y, scale int, clr color.Color) {
	x += (ScreenWidth - textWidth(str)*scale) / 2
	y += arcadeFontBaseSize * scale
	text.Draw(rt, str, getArcadeFonts(scale), x, y, clr)
}
