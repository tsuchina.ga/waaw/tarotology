package core

const (
	SideUp   = 1
	SideDown = 2
)

type Card struct {
	Number int
	Side   int
	Name   string
}

func (c *Card) Spin() {
	if c.Side == SideUp {
		c.Side = SideDown
	} else {
		c.Side = SideUp
	}
}

type CardSet struct {
	Cards []Card
}

func (c *CardSet) Create() {
	c.Cards = []Card{}
	c.Cards = append(c.Cards, Card{Number: 0, Side: SideUp, Name: "THE FOOL"})
	c.Cards = append(c.Cards, Card{Number: 1, Side: SideUp, Name: "THE MAGICIAN"})
	c.Cards = append(c.Cards, Card{Number: 2, Side: SideUp, Name: "THE HIGH PRIESTESS"})
	c.Cards = append(c.Cards, Card{Number: 3, Side: SideUp, Name: "THE EMPRESS"})
	c.Cards = append(c.Cards, Card{Number: 4, Side: SideUp, Name: "THE EMPEROR"})
	c.Cards = append(c.Cards, Card{Number: 5, Side: SideUp, Name: "THE HIEROPHANT"})
	c.Cards = append(c.Cards, Card{Number: 6, Side: SideUp, Name: "THE LOVERS"})
	c.Cards = append(c.Cards, Card{Number: 7, Side: SideUp, Name: "THE CHARIOT"})
	c.Cards = append(c.Cards, Card{Number: 8, Side: SideUp, Name: "STRENGTH"})
	c.Cards = append(c.Cards, Card{Number: 9, Side: SideUp, Name: "THE HERMIT"})
	c.Cards = append(c.Cards, Card{Number: 10, Side: SideUp, Name: "WHEEL OF FORTUNE"})
	c.Cards = append(c.Cards, Card{Number: 11, Side: SideUp, Name: "THE JUSTICE"})
	c.Cards = append(c.Cards, Card{Number: 12, Side: SideUp, Name: "THE HANGED MAN"})
	c.Cards = append(c.Cards, Card{Number: 13, Side: SideUp, Name: "DEATH"})
	c.Cards = append(c.Cards, Card{Number: 14, Side: SideUp, Name: "TEMPERANCE"})
	c.Cards = append(c.Cards, Card{Number: 15, Side: SideUp, Name: "THE DEVIL"})
	c.Cards = append(c.Cards, Card{Number: 16, Side: SideUp, Name: "THE TOWER"})
	c.Cards = append(c.Cards, Card{Number: 17, Side: SideUp, Name: "THE STAR"})
	c.Cards = append(c.Cards, Card{Number: 18, Side: SideUp, Name: "THE MOON"})
	c.Cards = append(c.Cards, Card{Number: 19, Side: SideUp, Name: "THE SUN"})
	c.Cards = append(c.Cards, Card{Number: 20, Side: SideUp, Name: "JUDGEMENT"})
	c.Cards = append(c.Cards, Card{Number: 21, Side: SideUp, Name: "THE WORLD"})
}

