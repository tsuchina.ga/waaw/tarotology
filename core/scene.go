package core

import "github.com/hajimehoshi/ebiten"

type Scene interface {
	Update(r *ebiten.Image, s *SceneManager) error
}

type SceneManager struct {
	current Scene
	next    Scene
}

func (s *SceneManager) GoTo(scene Scene) {
	if s.current == nil {
		s.current = scene
	} else {
		s.next = scene
	}
}

func (s *SceneManager) Update(r *ebiten.Image) error {
	s.current.Update(r, s)

	if s.next != nil {
		s.current = s.next
		s.next = nil
	}

	return nil
}
