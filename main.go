package main

import (
	"log"
	"gitlab.com/tsuchina.ga/waaw/tarotology/core"
	"github.com/hajimehoshi/ebiten"
)

func main() {
	c := &core.Core{}
	c.DebugOn()

	update := c.Update
	if err := ebiten.Run(update, core.ScreenWidth, core.ScreenHeight, 2, "tarotology"); err != nil {
		log.Fatalln(err)
	}
}
